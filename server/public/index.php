<?php

//Homework1
$name = 'Oleg';
echo $name . "\n";
$age = 19;
echo $age . "\n";
$pi = 3.14;
echo $pi . "\n";
$arr1 = ['alex', 'vova', 'tolya'];
print $arr1 . '\n';
//print_r($arr1);
$arr2 = ['alex', 'vova', 'tolya',
    ['kostya', 'olya']
];
print $arr2 . '\n';
//print_r($arr2);
$arr3 = ['alex', 'vova', 'tolya',
    ['kostya', 'olya',
        ['gosha', 'mila']
    ]
];
print $arr3 . '\n';
//print_r($arr3);
$arr4 = [
    ['Alex', 'Vova', 'Tolya'],
    ['Kostya', 'Olya'],
    ['Gosha', 'Mila']
];
print $arr4 . '\n';
//print_r($arr4);

//Homework2
$a = 42;
$b = 55;
echo $a < $b ? $b . 'больше $a' : $b . 'меньше $a';
echo '<br>';
$a = rand(5, 15);
$b = rand(5, 15);
echo $a < $b ? $b . 'больше $a' : $b . 'меньше $a';
echo '<br>';
$name = 'Oleg';
$surname = 'Balyasniy';
$patronymic = 'Nikolaevich';
echo $name . $surname[0] . '.' . $patronymic[0] . '.';
echo '<br>';

$number = 7;
$randNumber = rand(1, 99999);
$countNum = substr_count($randNumber, $number);
echo $number . 'встречается ' . $countNum . 'раз в числе ' . '$randNumber' . '<br>';

$a = 3;
echo $a . '<br>';

$a = 10;
$b = 2;
echo $a . ' + ' . $b . '=' . ($a + $b) . '<br>';
echo $a . ' - ' . $b . '=' . ($a - $b) . '<br>';
echo $a . ' * ' . $b . '=' . ($a * $b) . '<br>';
echo $a . ' % ' . $b . '=' . ($a % $b) . '<br>';

$c = 15;
$d = 2;
$result = $c + $d;
echo $c . ' + ' . $d . ' = ' . $result . '<br>';

$a = 10;
$b = 2;
$c = 5;
echo $a . ' + ' . $b . ' + ' . $c . '=' . ($a + $b + $c) . '<br>';

$a = 17;
$b = 10;
$c = $a - $b;
$d = $c;
$result = $c + $d;
echo $result . '<br>';

$text = 'Привет, Мир!';
echo $text . '<br>';

$text1 = 'Привет, ';
$text2 = 'Мир!';
echo $text1 . $text2 . '<br>';

$minutesInHour = 60;
$secInHour = 60 * $minutesInHour;
$secInDay = 24 * $secInHour;
$secInWeek = 7 * $secInDay;
$secInMonth = 30 * $secInDay;
echo 'Секунд в 1 часе: ' . $secInHour . 'Секунд в 1 сутках: ' . $secInDay . 'Секунд в 1 неделе: ' . $secInWeek . 'Секунд в 1 месяце : ' . $secInMonth . '<br>';

$var = 1;
$var += 12;
$var -= 14;
$var *= 5;
$var /= 7;
++$var;
--$var;
echo $var . '<br>';


$sec = date('s');
$minute = date('i');
$hour = date('h') + 3;
echo 'The current time: ' . $hour . ':' . $minute . ':' . $sec . '<br>';

$text = 'Я';
$text .= ' хочу';
$text .= ' знать';
$text .= ' PHP!';
echo $text . '<br>';


$foo = 'bar';
$bar = 10;
echo $$foo;
echo '<br>';

$a = 2;
$b = 4;
echo ($a++) + $b; // 6
echo $a + (++$b); // 8
echo (++$a) + ($b++); // 9
echo '<br>';

$a = 'Nix';
echo '<br>';
echo isset($a) ? 'переменная существует' : 'переменная не существует';

$a = 20;
echo '<br>';
echo gettype($a) == 'int' ? 'тип переменной int' : 'не int';

$a = null;
echo '<br>';
echo is_null($a) ? 'переменная со значением NULL' : 'переменной присвоено значение';

$a = 235;
echo '<br>';
echo is_integer($a) ? 'целое число' : 'не целое число';

$a = 19.45;
echo '<br>';
echo is_double($a) ? 'число с плавающей точкой' : 'не число с плавающей точкой';

$a = 'Name';
echo '<br>';
echo is_string($a) ? 'переменная строка' : 'переменная не строка';

$a = '856';
echo '<br>';
echo is_numeric($a) ? 'переменная число' : 'переменная не число';

$a = false;
echo '<br>';
echo is_bool($a) ? 'булевая переменная' : 'не булевая переменная';

$a = true;
echo '<br>';
echo is_scalar($a) ? 'переменная со скалярным значением' : 'переменная не со скалярным значением';

$a = 45.56778;
echo '<br>';
echo is_scalar($a) ? 'переменная со скалярным значением' : 'переменная не со скалярным значением';

$a = 'abc';
echo '<br>';
echo is_null($a) ? 'переменная NULL' : 'переменная не NULL';

$a = [1, 2, 3];
echo '<br>';
echo is_array($a) ? 'переменная массив' : 'переменная не массив';

$a = (object)'Nix';
echo '<br>';
echo is_object($a) ? 'переменная объект' : 'переменная не объект';
echo '<br>';
$a = 123;
$b = 234;
echo $a + $b;
echo '<br>';
$a = 345;
$b = 98;
echo ($a ** 2) + ($b ** 2);
echo '<br>';
$arr = [1, 7, 19];
$arr_count = count($arr);
echo array_sum($arr) / $arr_count;
echo '<br>';
$a = 8;
$b = 32;
$c = 16;
echo (++$a) - (2 * ($c - ($a * 2) + $b));
echo '<br>';
$x = 122;
echo $x % 3 . '<br>';
echo $x % 5 . '<br>';

$y = 48;
echo ($y * 0.3) + $y . '<br>';
echo ($y * 1.2) + $y . '<br>';
$q = 80;
$w = 150;
echo ($q * 0.4) + ($w * 0.85) . '<br>';

$a = 123;
$numArr = str_split($a);
echo array_sum($numArr) . '<br>';

$a = 123;
$a = (string)$a;
$a[1] = 0;
echo (int)$a . '<br>';
$arrNum = array_reverse(str_split($a));
echo (int)implode('', $arrNum) . '<br>';

$a = 575;
echo ($a % 2) == 0 ? 'четное число' : 'нечетное число';

//Homework4

/*
* 1) Написать программу, которая выводит простые числа, т.е. делящиеся без
остатка только на себя и на 1
*/
$number = 0;
for ($i = 1; $i <= 100; $i++) {
    for ($j = 1; $j <= $i; $j++) {
        if ($i % $j == 0) {
            $number++;
        }
    }
    if ($number <= 2) {
        echo $i . '<br>';
    }
    $number = 0;
};

/*
*2) Сгенерируйте 100 раз новое число и выведите на экран количество четных чисел из этих 100.
*/

$y = 0;
for ($j = 1; $j <= 100; $j++) {
    if ($j % 2 == 0) {
        $y++;
    }
}
echo 'In one hundred' . $y . 'even numbers';

/*
* 3) Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз сгенерировались эти числа (1, 2, 3, 4 и 5).
*/
$first = 0;
$second = 0;
$third = 0;
$fourth = 0;
$fifth = 0;
for ($i = 1; $i <= 100; $i++) {
    $randomNumber = rand(1, 5);
    if ($randomNumber == 1) {
        $first++;
    } elseif ($randomNumber == 2) {
        $second++;
    } elseif ($randomNumber == 3) {
        $third++;
    } elseif ($randomNumber == 4) {
        $fourth++;
    } elseif ($randomNumber == 5) {
        $fifth++;
    }
}

echo 'The number 1 was displayed  times' . $first . '<p>The number 2 was displayed  times $second</p>' . '<p>The number 3 was displayed  times</p>' . $third . '<p>The number 4 was displayed  times $fourth</p>' . '<p>The number 5 was displayed  times $fifth</p>';
/*
* 4) Используя условия и циклы сделать таблицу в 5 колонок и 3 строки (5x3), отметить разными цветами часть ячеек.
*/

echo '<table>';
for ($i = 0; $i < 3; $i++) {
    echo '<tr>';
    for ($j = 0; $j < 5; $j++) {
        $r = rand(0, 255);
        $g = rand(0, 255);
        $b = rand(0, 255);
        echo '<td style="background-color:rgb(' . $r . ',' . $g . ',' . $b . ');">&nbps</td>';
    }
    echo '</tr>';
}
echo '</table>';
/*
* 1) В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую пору года попадает этот месяц (зима, лето, весна, осень).
*/

$month = rand(1, 12);
if ($month == 1 || $month == 2 || $month == 12) {
    echo '<p>This is winter</p>';
}
if ($month == 3 || $month == 4 || $month == 5) {
    echo '<p>This is spring</p>';
}
if ($month == 6 || $month == 7 || $month == 8) {
    echo '<p>This is summer</p>';
}
if ($month == 9 || $month == 10 || $month == 11) {
    echo '<p>This is autumn</p>';
}

/*
* 2) Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым символом этой строки является буква 'a'. Если это так - выведите 'да', в противном случае выведите 'нет'.
*/
$str = 'abcde';
if ($str[0] == 'a') {
    echo '<p>Да</p>';
} else {
    echo '<p>Нет</p>';
}
/*
* 3)Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки является цифра 1, 2 или 3. Если это так - выведите 'да', в противном случае выведите 'нет'.
*/
$str = '12345';
if ($str[0] == '1' || $str[0] == 2 || $str[0] == 3) {
    echo '<p>Да</p>';
} else {
    echo '<p>Нет</p>';
}
/*
* 4)Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при test, равном true, false. Напишите два варианта скрипта - тернарка и if else.
*/
$test = true;
echo $test == true ? 'Верно' : 'Неверно';
if ($test == true) {
    echo '<p>Верно</p>';
} else {
    echo '<p>Неверно</p>';
}
/*
* 5) Дано Два массива рус и англ ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
Если переменная lang = ru вывести массив на русском языке, а если en то вывести на английском языке. Сделат через if else и через тернарку.
*/
$english = ['mon', 'tue', 'wen', 'thu', 'fri', 'sat', 'sun'];
$rus = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$lang = 'ru';
if ($lang == 'ru') {
    print $rus;
} else {
    print $english;
}
echo $lang == 'ru' ? print $rus : print $english;
/*
* 6) В переменной cloсk лежит число от 0 до 59 – это минуты. Определите в какую четверть часа попадает это число (в первую, вторую, третью или четвертую). тернарка и if else.
*/
$clock = rand(0, 59);
if ($clock <= 15) {
    echo '<p>First quarter</p>';
} elseif ($clock > 15 && $clock <= 30) {
    echo '<p>Second quarter</p>';
} elseif ($clock > 30 && $clock <= 45) {
    echo '<p>Third quarter</p>';
} elseif ($clock > 45 && $clock <= 59) {
    echo '<p>Fourth quarter</p>';
} else {
    echo '<p>Try again</p>';
}
echo $clock <= 15 ? 'First quarter' :
    ($clock > 15 && $clock <= 30 ? 'Second quarter' :
        ($clock > 30 && $clock <= 45 ? 'Third quarter' :
            ($clock > 45 && $clock <= 59 ? 'Fourth quarter' : 'Try again')));
echo '<br>';
/*
* Все задания делаем
while
do while
for
foreach

не используй готовые функции

1. Дан массив
['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
Развернуть этот массив в обратном направлении
*/
$arrPeople = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrPeopleRevers = [];
$i = 0;
$j = 0;
//FOR
for ($i; $arrPeople[$i]; $i++) {
    $j = $i;
}
for ($j; $j >= 0; $j--) {
    $arrPeopleRevers[] = $arrPeople[$j];
}
print $arrPeopleRevers;
echo '<br>';

//WHILE

while (isset($arrPeople[$i])) {
    $j = $i;
    $i++;
}
while ($j >= 0) {
    $arrPeopleRevers[] = $arrPeople[$j];
    $j--;
}
print $arrPeopleRevers;
echo '<br>';

//Do while
$arrPeople = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrPeopleRevers = [];
$i = 0;
$j = 0;
do {
    $j = $i;
    $i++;
} while (isset($arrPeople[$i]));
do {
    $arrPeopleRevers[] = $arrPeople[$j];
    $j--;
} while ($j >= 0);
print $arrPeopleRevers;
echo '<br>';

//Foreach
$arrPeople = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrPeopleRevers = [];
$i = 0;
foreach ($arrPeople as $key => $value) {
    $i++;
}
foreach ($arrPeople as $key => $value) {
    $arrPeopleRevers[] = $arrPeople[$i - 1];
    $i--;
}
print $arrPeopleRevers;
echo '<br>';

/*2. Дан массив
[44, 12, 11, 7, 1, 99, 43, 5, 69]
Развернуть этот массив в обратном направлении
*/
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$arrNumberRevers = [];
$i = 0;
$j = 0;
//for
for ($i; $arrNumber[$i]; $i++) {
    $j = $i;
}
for ($j; $j >= 0; $j--) {
    $arrNumberRevers[] = $arrNumber[$j];
}
print $arrNumberRevers;
echo '<br>';

//while

while (isset($arrNumber[$i])) {
    $j = $i;
    $i++;
}
while ($j >= 0) {
    $arrNumberRevers[] = $arrNumber[$j];
    $j--;
}
print $arrNumberRevers;
echo '<br>';

//do while
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$arrNumberRevers = [];
$i = 0;
$j = 0;
do {
    $j = $i;
    $i++;
} while (isset($arrNumber[$i]));
do {
    $arrNumberRevers[] = $arrNumber[$j];
    $j--;
} while ($j >= 0);
print $arrNumberRevers;
echo '<br>';

//foreach
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$arrNumberRevers = [];
$i = 0;
$j = 0;
$arrNumberRevers = [];
foreach ($arrNumber as $key => $value) {
    $i++;
}
foreach ($arrNumber as $key => $value) {
    $arrNumberRevers[] = $arrNumber[$i - 1];
    $i--;
}
print $arrNumberRevers;
echo '<br>';

/*
 * 3. Дана строка
    let str = 'Hi I am ALex'
    развенуть строку в обратном направлении.
 */

$str = 'Hi I am ALex';

//for
$i = 0;
$j = 0;
$arrLetter = [];
for ($i; $str[$i]; $i++) {
    $j = $i;
}
for ($j; $j >= 0; $j--) {
    $arrLetter[] = $str[$j];
}
print $arrLetter;
echo '<br>';

//foreach
$str = 'Hi I am ALex';
$i = 0;
$strRevers = [];
while (isset($str[$i])) {
    $strRevers[] = $str[$i];
    $i++;
}
$stringReverse = '';
foreach ($strRevers as $key => $value) {
    $i--;
    $stringReverse .= $strRevers[$i] . '<br>';
}
echo $stringReverse;
echo '<br>';

//while
$i = 0;
$j = 0;
$arrLetter = [];
while (isset($str[$i])) {
    $j = $i;
    $i++;
}
while ($j >= 0) {
    $arrLetter[] = $str[$j];
    $j--;
}
print $arrLetter;
echo '<br>';

// do while
$i = 0;
$j = 0;
$arrLetter = [];
do {
    $j = $i;
    $i++;
} while (isset($str[$i]));
do {
    $arrLetter[] = $str[$j];
    $j--;
} while ($j >= 0);
print $arrLetter;
echo '<br>';

/*
 * 4. Дана строка. готовую функцию toUpperCase() or tolowercase()
let str = 'Hi I am ALex'
сделать ее с с маленьких букв
 */


echo mb_strtolower($str);
echo '<br>';
/* If i can't use it, then here :
*/

//for
$str = 'Hi I am ALex';
$strSmall = '';
$i = 0;
$j = 0;
for ($i; isset($str[$i]); $i++) {
    $j = $i;
}
for ($j = 0; $j <= $i; $j++) {
    $strSmall .= mb_strtolower($str[$j]);
}
echo $strSmall;
echo '<br>';

//while
while (isset($str[$i])) {
    $strSmall .= mb_strtolower($str[$i]);
    $i++;
}
echo $strSmall;
echo '<br>';

//do while
$str = 'Hi I am ALex';
$i = 0;
$strSmall = '';
do {
    $strSmall .= mb_strtolower($str[$i]);
    $i++;
} while (isset($str[$i]));
echo $strSmall;
echo '<br>';

//foreach
$str = 'Hi I am ALex';
$strSmall = '';
$i = 0;
$j = 0;
$arrStr = [];
while (isset($str[$i])) {
    $arrStr[] = $str[$i];
    $i++;
}
foreach ($arrStr as $key => $value) {
    $strSmall .= mb_strtolower($value);
}
echo $strSmall;
echo '<br>';

/*
 * 5. Дана строка
let str = 'Hi I am ALex'
сделать все буквы большие
 */

$str = 'Hi I am ALex';
echo mb_strtoupper($str);
echo '<br>';
/* If i can't use it, then here :
*/

//for
$strBig = '';
for ($i; isset($str[$i]); $i++) {
    $j = $i;
}
for ($j = 0; $j <= $i; $j++) {
    $strBig .= mb_strtoupper($str[$j]);
}
echo $strBig;
echo '<br>';

//while
while (isset($str[$i])) {
    $strBig .= mb_strtoupper($str[$i]);
    $i++;
}
echo $strBig;
echo '<br>';

//do while
do {
    $strBig .= mb_strtoupper($str[$i]);
    $i++;
} while (isset($str[$i]));
echo $strBig;
echo '<br>';

//foreach
$str = 'Hi I am ALex';
$i = 0;
$j = 0;
$strBig = '';
$arrStrBig = [];
while (isset($str[$i])) {
    $arrStrBig[] = $str[$i];
    $i++;
}
foreach ($arrStrBig as $key => $value) {
    $strBig .= mb_strtoupper($value);
}
echo $strBig;
echo '<br>';

/*
 * 6. Дана строка
let str = 'Hi I am ALex'
развернуть ее в обратном направлении
 */

$str = 'Hi I am ALex';

//for
$i = 0;
$j = 0;
$arrLetter = [];
for ($i; $str[$i]; $i++) {
    $j = $i;
}
for ($j; $j >= 0; $j--) {
    $arrLetter[] = $str[$j];
}
print $arrLetter;
echo '<br>';

//foreach
$str = 'Hi I am ALex';
$i = 0;
$strRevers = [];
while (isset($str[$i])) {
    $strRevers[] = $str[$i];
    $i++;
}
$stringReverse = '';
foreach ($strRevers as $key => $value) {
    $i--;
    $stringReverse .= $strRevers[$i] . '<br>';
}
echo $stringReverse;
echo '<br>';

//while
$i = 0;
$j = 0;
$arrLetter = [];
while (isset($str[$i])) {
    $j = $i;
    $i++;
}
while ($j >= 0) {
    $arrLetter[] = $str[$j];
    $j--;
}
print $arrLetter;
echo '<br>';

// do while
$i = 0;
$j = 0;
$arrLetter = [];
do {
    $j = $i;
    $i++;
} while (isset($str[$i]));
do {
    $arrLetter[] = $str[$j];
    $j--;
} while ($j >= 0);
print $arrLetter;
echo '<br>';

/*
 * 7. Дан массив
['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
сделать все буквы с маленькой
 */

//for
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSmall = [];
for ($i = 0; isset($arr[$i]); $i++) {
    $arrSmall[] = mb_strtolower($arr[$i]);
}
print $arrSmall;
echo '<br>';

//while
$i = 0;
$arrSmall = [];
while (isset($arr[$i])) {
    $arrSmall[] = mb_strtolower($arr[$i]);
    $i++;
}
print $arrSmall;
echo '<br>';

//do while
$i = 0;
$arrSmall = [];
do {
    $arrSmall[] = mb_strtolower($arr[$i]);
    $i++;
} while (isset($arr[$i]));
print $arrSmall;
echo '<br>';

//foreach
$arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$arrSmall = [];
foreach ($arr as $key => $value) {
    $arrSmall[] = mb_strtolower($value);
}
print $arrSmall;
echo '<br>';

/*
 * 9. Дано число
let num = 1234678
развернуть ее в обратном направлении
 */

//for
$num = 1234678;
$i = -1;
$numRevers = '';
$num = (string)$num;
for ($i = -1; isset($num[$i]); $i--) {
    $numRevers .= $num[$i];
}
$numRevers = (int)$numRevers;
echo $numRevers;
echo '<br>';

//while
$num = (string)$num;
while (isset($num[$i])) {
    $numRevers .= $num[$i];
    $i--;
}
$numRevers = (int)$numRevers;
echo $numRevers;
echo '<br>';

//do while
$num = (string)$num;
do {
    $numRevers .= $num[$i];
    $i--;
} while (isset($num[$i]));
$numRevers = (int)$numRevers;
echo $numRevers;
echo '<br>';

//foreach
$num = (string)$num;
$i = 0;
while (isset($num[$i])) {
    $numArr[] = $num[$i];
    $i++;
}
$i = -1;
foreach ($numArr as $value) {
    $numRevers .= $numArr[$i];
    $i--;
}
$numRevers = (int)$numRevers;
echo $numRevers;
echo '<br>';

/*
 * 10. Дан массив
[44, 12, 11, 7, 1, 99, 43, 5, 69]
отсортируй его в порядке убывания
 */
//for
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$count = 0;
while (isset($arr[$count])) {
    $count++;
}
for ($i = 0; $i < $count; $i++) {
    for ($j = 0; $j < $count - 1; $j++) {
        if ($arr[$j] > $arr[$j + 1]) {
            $number = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $number;
        }
    }
}
print $arr;
echo '<br>';

//while
while (isset($arr[$i])) {
    $count = $i + 1;
    $i++;
}
while ($i < $count) {
    while ($j < $count - 1) {
        if ($arr[$j] > $arr[$j + 1]) {
            $number = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $number;
        }
        $j++;
    }
    $i++;
    if ($j == $count - 1) {
        $j = 0;
    }
}
print $arr;
echo '<br>';

//do while
$count = 0;
$i = 0;
do {
    $count = $i + 1;
    $i++;
} while (isset($arr[$i]));
$i = 0;
$j = 0;
do {
    $i++;
    if ($j == $count - 1) {
        $j = 0;
    }
    do {
        if ($arr[$j] > $arr[$j + 1]) {
            $number = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $number;
        }
        $j++;
    } while ($j < $count - 1);
} while ($i < $count);
print $arr;
echo '<br>';

//foreach
while (isset($arr[$count])) {
    $count++;
}
foreach ($arr as $key => $value) {
    for ($j = 0; $j < $count - 1; $j++) {
        if ($arr[$j] > $arr[$j + 1]) {
            $number = $arr[$j + 1];
            $arr[$j + 1] = $arr[$j];
            $arr[$j] = $number;
        }
    }
}
print $arr;
echo '<br>';
/*?>
//Homework5

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php */
/*
  1.Вам нужно создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand). Далее, вычислить произведение тех элементов, которые больше нуля и у которых четные индексы. После вывести на экран элементы, которые больше нуля и у которых нечетный индекс.
*/
/**
 * @param integer $first
 * @param integer $last
 * @param integer $lenght
 * @return array
 */
function randArray($first, $last, $lenght): array
{
    $arrRandResult = [];
    for ($i = 0; $i < $lenght; $i++) {
        $arrRandResult[] = rand($first, $last);
    }
    return $arrRandResult;
}

/**
 * @param integer $arrRandResult
 * @return integer
 */
function productOfNumbers($arrRandResult)
{
    $res = 1;
    foreach ($arrRandResult as $value) {
        if ($value > 0 && ($value % 2) == 0) {
            $res *= $value;
        }
    }
    return $res;
}

/**
 * @param array $arrRandResult
 * @return string|null
 */
function oddNumbers(array $arrRandResult)
{
    $oddNumbersResult = null;
    foreach ($arrRandResult as $value) {
        if ($value > 0 && ($value % 2) == 1) {
            $oddNumbersResult = $oddNumbersResult . $value;
        }
    }
    return $oddNumbersResult;
}


$arrRandResult = randArray(1, 100, 3);
echo productOfNumbers($arrRandResult);
echo '<br>';
echo oddNumbers($arrRandResult);
echo '<br>';

/*
 * 2.Даны два числа. Найти их сумму и произведение. Даны два числа. Найдите сумму их квадратов.
 */
/**
 * @param integer $first
 * @param integer $second
 * @return integer
 */
function sumEvenNumber($first, $second)
{
    $sum = $first + $second;
    $evenNumber = $first * $second;
    echo 'Amount numbers = ' . $sum . '<br>';
    echo 'Even numbers = ' . $evenNumber . '<br>';
    return 'Amount numbers = ' . $sum . '<br>' . 'Even numbers ' . $evenNumber . '<br>';

}

/**
 * @param integer $first
 * @param integer $second
 * @return integer
 */
function sumOfTheSquares($first, $second)
{
    $squars = ($first * $first) + ($second * $second);
    return $squars;
}

sumEvenNumber(3, 7);
sumOfTheSquares(3, 7);

/*
 * 3.Даны три числа. Найдите их среднее арифметическое.
 */
/**
 * @param integer $firstNumber
 * @param integer $secondNumber
 * @param integer $thirdNumber
 * @return integer
 */
function averageOfNumbers($firstNumber, $secondNumber, $thirdNumber)
{
    $average = ($firstNumber + $secondNumber + $thirdNumber) / 3;
    return $average;
}

averageOfNumbers(7, 11, 8);

/*
 * 4.Дано число. Увеличьте его на 30%, на 120%.
 */
/**
 * @param integer $number
 * @param integer $percent
 * @return integer
 */
function increaseTheNumber($number, $percent)
{
    $percentOfNumber = $percent;
    $count = $number;
    $res = $count / 100 * $percentOfNumber + $count;
    return $res;
}

increaseTheNumber(100, 30);
increaseTheNumber(100, 120);
/*
 * 5.Пользователь выбирает из выпадающего списка страну (Турция, Египет или Италия), вводит количество дней для отдыха и указывает, есть ли у него скидка (чекбокс). Вывести стоимость отдыха, которая вычисляется как произведение   количества дней на 400. Далее это число увеличивается на 10%, если выбран Египет, и на 12%, если выбрана Италия. И далее это число уменьшается на 5%, если указана скидка.
?>
    <p>Choose your trip!</p>
    <form method="post" action="index.php">
        <select name="chooseCountry" size="1" required>
            <option value="Turkey">Turkey</option>
            <option value="Egypt">Egypt</option>
            <option value="Italy">Italy</option>
        </select>
        <input type="number" name="chooseDays" placeholder="Enter the number of days" required>
        <input type="checkbox" value="promo" name="discount">Promocode
        <button type="submit">Send</button>
    </form>
<?php
*/
$country = $_POST['chooseCountry'];
$days = $_POST['chooseDays'];
$discount = $_POST['discount'];
if (isset($country)) {
    /**
     * @param integer $country
     * @param integer $days
     * @param integer $discount
     * @return integer
     */
    function restCost($country, $days, $discount)
    {
        $price = $days * 400;
        if ($country == 'Egypt') {
            $price = $price / 100 * 10 + $price;
        } elseif ($country == 'Italy') {
            $price = $price / 100 * 12 + $price;
        }
        echo $discount == 'promo' ? 'Your price is ' . ($price -= 5 * $price / 100) : 'Your price is $price';
        return $price;
    }

    restCost($country, $days, $discount);
}
/*
 * 6.Пользователь вводит свой имя, пароль, email. Если вся информация указана, то показать эти данные после фразы 'Регистрация прошла успешно', иначе сообщить какое из полей оказалось не заполненным.
 */
/*?>
    <p>Register please</p>
    <form action="index.php" method="post">
        <input type="text" name="name" placeholder="Enter your name">
        <input type="text" name="email" placeholder="Enter your email">
        <input type="text" name="password" placeholder="Enter your password">
        <button type="submit">Send</button>
    </form>
<?php*/
$name = $_POST['name'];
$email = $_POST['email'];
$password = $_POST['password'];
/**
 * @param string $name
 * @param string $email
 * @param string $password
 * @return string
 */
function validationForm($name, $email, $password)
{
    $errors = [];
    $rightValidated = '';
    if (!$name) {
        $errors[$name] .= '<p>You did not enter your name</p>';
    }
    if (!$email) {
        $errors[$email] .= '<p>You did not enter your email</p>';

    }
    if (!$password) {
        $errors[$password] .= '<p>You did not enter your password</p>';
    }
    if (empty($errors)) {
        echo '<p>Registration completed successfully </p>' . '<p>You name is $name</p>' .
            '<p>Your email is $email</p>' . '<p>Your password $password</p>';
    } else {
        foreach ($errors as $value) {
            $rightValidated .= $value;
        }
    }
    echo $rightValidated;
    return $rightValidated;
}

validationForm($name, $email, $password);
/*?>
    <!-- 7.Выведите на экран n раз фразу "Silence is golden". Число n вводит пользователь на форме. Если n некорректно, вывести фразу "Bad n".
    -->
    <p>Enter your number(min = 1,max = 5)</p>
    <form action="index.php" method="post">
        <input name="number" type="text">
        <button type="submit">Send</button>
    </form>
<?php*/
$number = $_POST['number'];
/**
 * @param integer $clientNumber
 * @return boolean|void
 */
function countSilence($clientNumber)
{
    if ($clientNumber <= 5) {
        for ($i = 0; $i < $clientNumber; $i++) {
            return true;
        }
    } elseif ($clientNumber > 5 && $clientNumber <= 0) {
        return false;
    } else {
        return false;
    }
}

countSilence($number);
/*
 * 8.Заполнить массив длины n нулями и единицами, при этом данные значения чередуются, начиная с нуля.
 */
$nLenth = rand(1, 50);
$first = rand(1, 100);
$end = rand(1, 100);
/**
 * @param integer $start
 * @param integer $end
 * @param integer $lenghtN
 * @return array
 */
function arrCreateLenght($start, $end, $lenghtN): array
{
    $firstNum = $start;
    $arr[] = $start;
    for ($i = 0; $i < $lenghtN; $i++) {
        if ($firstNum == $start) {
            $firstNum = $end;
            $arr[] = $end;
        } else {
            $firstNum = $start;
            $arr[] = $start;
        }
    }
    return $arr;
}

print arrCreateLenght($first, $end, $nLenth);

/*
 * 9.Определите, есть ли в массиве повторяющиеся элементы.
 */
$arrRepeat = arrCreateLenght($first, $end, $nLenth);
/**
 * @param array $arr
 * @return array
 */
function repeatElements(array $arr): array
{
    $arrResult = [];
    foreach ($arr as $value) {
        if ($value == $value) {
            $arrResult[$value]++;
        }
    }
    return $arrResult;
}

foreach (repeatElements($arrRepeat) as $key => $value) {
    if ($value > 1) {
        echo $key . 'Repeated' . $value . 'times';
    }
}

repeatElements($arrRepeat);

/*
 * 10.Найти минимальное и максимальное среди 3 чисел
 */
/**
 * @param array $array
 * @return array
 */
function minNumber(array $array): array
{
    $arrMinMax = $array;
    $arrMinLenght = count($arrMinMax);
    $minNumber = $array[0];
    for ($i = 1; $i < $arrMinLenght; $i++) {
        if ($minNumber > $array[$i]) {
            $minNumber = $array[$i];
        }
    }
    return $minNumber;
}

/**
 * @param array $array
 * @return array
 */
function maxNumber(array $array): array
{
    $arrMaxLenght = count($array);
    $maxNumber = $array[0];
    for ($i = 1; $i < $arrMaxLenght; $i++) {
        if ($maxNumber < $array[$i]) {
            $maxNumber = $array[$i];
        }
    }
    return $maxNumber;
}


print minNumber($arrRepeat);
echo '<br>';
print maxNumber($arrRepeat);

/*
 * 11.Найти площадь
 */

$i = rand(1, 100);
$j = rand(1, 100);
/**
 * @param integer $x
 * @param integer $y
 * @return integer
 */
function square($x, $y)
{
    $z = $x * $y;
    return $z;
}
